package model.data_structures;

public class NodoDoble<T> {
	
	private T objeto;
	private NodoDoble<T> siguiente;
	private NodoDoble<T> anterior;
	
	public NodoDoble (T NObjeto)
	{
		objeto = NObjeto;
		siguiente = null;
		anterior = null;
	}
	
	public void setSiguiente(NodoDoble<T> siguiente )
	{
		this.siguiente = siguiente;
	}
	
	public void setAnterior(NodoDoble<T> anterior )
	{
		this.anterior = anterior;
	}

	public NodoDoble<T> getSiguiente()
	{
		return siguiente;
	}
	
	public NodoDoble<T> getAnterior()
	{
		return anterior;
	}
	
	public void setObjeto( T objeto )
	{
		this.objeto = objeto; 
	}
	
	public T getObjeto()
	{
		return objeto;
	}


}
