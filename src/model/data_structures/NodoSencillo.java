package model.data_structures;

public class NodoSencillo<T> {

	private T item;
	private NodoSencillo<T> nodoSiguiente;
	
	public NodoSencillo(T pObjeto){
		item = pObjeto;
		nodoSiguiente = null;
	}
	
	public void asignarSiguiente(NodoSencillo<T> sig){
		nodoSiguiente = sig;
	}
	
	public T darObjeto(){
		return item;
	}
	
	public NodoSencillo<T> darSiguiente(){
		return nodoSiguiente;
	}
	
}
