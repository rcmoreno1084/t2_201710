package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	private int agnoActual= -1;
	
	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		peliculasAgno = new ListaEncadenada<VOAgnoPelicula>();
		int agnoOrden = 1950;
		while (agnoOrden <=2016){
			VOAgnoPelicula agregar = new VOAgnoPelicula();
			agregar.setAgno(agnoOrden);
			agregar.setPeliculas(new ListaEncadenada<VOPelicula>());
			peliculasAgno.agregarElementoFinal(agregar);
			agnoOrden++;
		}
		if (agnoOrden == 2017){
			VOAgnoPelicula agregar = new VOAgnoPelicula();
			agregar.setAgno(-1);
			agregar.setPeliculas(new ListaEncadenada<VOPelicula>());
			peliculasAgno.agregarElementoFinal(agregar);
			agnoOrden = -1;
		}
		
		try {
			FileReader fr = new FileReader(new File (archivoPeliculas));
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String s = br.readLine();
			misPeliculas = new ListaDobleEncadenada<VOPelicula>();
			while (s!= null) {

				//Procesar String
				String titulo = s;
				String id = titulo.substring(0, titulo.indexOf(","));

				String resto = titulo.substring(titulo.indexOf(",")+1, titulo.length());
				titulo = resto.substring(0, resto.lastIndexOf(","));


				int agno = -1;
				if (titulo.endsWith(")")&&Character.isDigit(titulo.charAt(titulo.length()-2)))
				{
					
					String agnoP =titulo.substring(titulo.lastIndexOf(" ")+1, titulo.length());
					agnoP = agnoP.substring(1);
					agnoP = agnoP.substring(0, 4);
					agno = Integer.parseInt(agnoP);
					titulo = titulo.substring(0,titulo.lastIndexOf(" "));

				}else if (titulo.endsWith(")\"")&&Character.isDigit(titulo.charAt(titulo.length()-3)))
				{
					
					String agnoP =titulo.substring(titulo.lastIndexOf(" ")+1, titulo.length());
					agnoP = agnoP.substring(1);
					agnoP = agnoP.substring(0, 4);
					agno = Integer.parseInt(agnoP);
					titulo = titulo.substring(0,titulo.lastIndexOf(" "))+"\"";

				}
				String genero = resto.substring(resto.lastIndexOf(",")+1);
				String generos[] = genero.split("|");

				//Lista de generos
				ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
				for(int i = 0; i<generos.length; i++){
					listaGeneros.agregarElementoFinal(generos[i]);
				}


				VOPelicula elem = new VOPelicula();
				elem.setTitulo(titulo);
				elem.setAgnoPublicacion(agno);
				elem.setGenerosAsociados(listaGeneros);
				misPeliculas.agregarElementoFinal(elem);
				
				darPeliculasAgno(agno).agregarElementoFinal(elem);
				//System.out.println("movie+");
				s =br.readLine();
			}
			br.close();
			fr.close();
			
			
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> listaPeliculasBusqueda = new ListaEncadenada<VOPelicula>();
		
		for(int i = 0; i<misPeliculas.darNumeroElementos(); i++)
		{
			String titulo = misPeliculas.darElemento(i).getTitulo();
			
			if(titulo.contains(busqueda))
			{
				listaPeliculasBusqueda.agregarElementoFinal(misPeliculas.darElemento(i));
			}

		}
		return listaPeliculasBusqueda;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub
		ILista<VOPelicula> listaPeliculasPorAgno = new ListaEncadenada<VOPelicula>();
		VOAgnoPelicula busqueda = encontrarVOAgno(agno);
		listaPeliculasPorAgno = busqueda.getPeliculas();
		agnoActual = agno;
		return listaPeliculasPorAgno;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		if(agnoActual == 2016){
			agnoActual = -1;
		}else if(agnoActual != -1) agnoActual++;
		return encontrarVOAgno(agnoActual);
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		if(agnoActual == -1){
			agnoActual = 2016;
		}else if (agnoActual != 1950) agnoActual --;
		return encontrarVOAgno(agnoActual);
	}
	
	
	/**
	 * Metodo extra que busca a�o entre los rangos dados: 1950-2016
	 * @param agno
	 * @return elemento VOAgnoPelicula encontrado, en caso de no estar en el rango se da el elemento de peliculas sin a�o.
	 */
	private VOAgnoPelicula encontrarVOAgno(int agno){
		Iterator<VOAgnoPelicula> iter = peliculasAgno.iterator();
		boolean encontrado = false;
		VOAgnoPelicula agnoInvalido = null;
		while (iter.hasNext() && !encontrado){
			VOAgnoPelicula busqueda = iter.next();
			if (busqueda.getAgno()==agno){
				return busqueda;
			}
			agnoInvalido = busqueda;
		}
		return agnoInvalido;
	}


}
