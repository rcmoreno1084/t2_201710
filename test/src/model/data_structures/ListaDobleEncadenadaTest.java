package model.data_structures;

import java.util.Iterator;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListaDobleEncadenadaTest {

	public String objeto = "cadena";
	public ListaDobleEncadenada<String> listaDoble;
	
	//Escenario 1
	public void SetUp()
	{
		listaDoble = new ListaDobleEncadenada<>();
		int size = 2;
		int pos = 0;
		while (pos < size)
		{
			pos++;
			listaDoble.agregarElementoFinal(objeto);
		}
	}
	
	//Escenario 2
	public void SetUp2()
	{
		listaDoble = new ListaDobleEncadenada<>();
		listaDoble.agregarElementoFinal("String1");
		listaDoble.agregarElementoFinal("String2");
	}
	
	//Escenario 3
	public void SetUp3()
	{
		listaDoble = new ListaDobleEncadenada<>();
		listaDoble.agregarElementoFinal("String1");
	}
	
	@Test //Test de Agregar Elemento al Final
	public void AgregarElementoFinalTest()
	{
		SetUp();
		listaDoble.agregarElementoFinal("ultimo elemento");
		assertEquals("ultimo elemento", listaDoble.darElemento(listaDoble.darNumeroElementos()-1));
	}
	
	@Test //Test de dar Elemento
	public void darElementoTest()
	{
		SetUp2();
		assertEquals("String2", listaDoble.darElemento(1));
	}
	
	@Test //Test de dar Elemento
	public void darElementoPosicionActualTest()
	{
		SetUp2();
		assertEquals(listaDoble.darElemento(0), listaDoble.darElementoPosicionActual());
	}
	
	@Test //Test de dar numero de elementos
	public void darNumeroElementosTest()
	{
		SetUp();
		assertEquals(2, listaDoble.darNumeroElementos());
	}
	
	@Test
	public void avanzarSiguientePosicionTest()
	{
		SetUp();
		assertEquals(true, listaDoble.avanzarSiguientePosicion());
		SetUp3();
		assertEquals(false, listaDoble.avanzarSiguientePosicion());
	}
	
	@Test
	public void retrocederPosicionAnteriorTest()
	{
		SetUp3();
		assertEquals(false, listaDoble.avanzarSiguientePosicion());
	}
	
	
	
	
	

}
