package model.data_structures;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

public class ListaEncadenadaTest {
	public String item = "cadena ";
	public ListaEncadenada<String> lista;

	
	@Test
	public void testIterator() {
		crearListaTest();
		Iterator<String> iter = lista.iterator();
		while (iter.hasNext()){
			String res = iter.next();
			System.out.println("testIterator: " + res);
		}
		
	}

	@Test
	public void testAgregarElementoFinal() {
		crearListaTest();
		lista.agregarElementoFinal("ultimo");
		System.out.println("testAgregarElementoFinal: " + lista.darElemento(lista.darNumeroElementos()-1));
		
	}

	@Test
	public void testDarElemento() {
		crearListaTest();
		System.out.println("testDarElemento: "+lista.darElemento(3));
		System.out.println("testDarElemento: "+lista.darElemento(7));
		System.out.println("testDarElemento: "+lista.darElemento(4));
		System.out.println("testDarElemento: "+lista.darElemento(11));
	}

	@Test
	public void testDarNumeroElementos() {
		crearListaTest();
		System.out.println("testDarNumeroElementos: " + lista.darNumeroElementos());
	}

	@Test //Implementa avanzar y retroceder poscicion.
	public void testDarElementoPosicionActual() {
		crearListaTest();
		lista.darElemento(6);
		lista.avanzarSiguientePosicion();
		lista.avanzarSiguientePosicion();
		System.out.println(lista.darElementoPosicionActual());
		
		lista.retrocederPosicionAnterior();
		lista.retrocederPosicionAnterior();
		lista.retrocederPosicionAnterior();
		lista.retrocederPosicionAnterior();
		System.out.println(lista.darElementoPosicionActual());
	}


	public void crearListaTest(){
		int idCadena = 0;
		lista = new ListaEncadenada<String>();
		while (idCadena < 15){
			lista.agregarElementoFinal(item + idCadena);
			idCadena++;
		}		
	}
	
}
